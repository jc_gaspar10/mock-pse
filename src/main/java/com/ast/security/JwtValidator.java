package com.ast.security;

import com.ast.model.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

@Component
public class JwtValidator {


    private String secret = "sp900853722";

    public JwtUser validate(String token) {

        JwtUser jwtUser = null;

        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            jwtUser = new JwtUser();

            jwtUser.setUsername(body.getSubject());
            jwtUser.setPassword( (String) body.get("password"));
            jwtUser.setGrant_type((String) body.get("grant_type"));
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: Manejar la excepción al momento de leer el token
        }

        return jwtUser;
    }
}
