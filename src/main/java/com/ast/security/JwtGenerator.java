package com.ast.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtGenerator {


    /**
     * Método encargado de devolver el token con la información dada
     *
     * @param username   usuario
     * @param password   contraseña del usuario
     * @param grant_type tipo de acceso
     * @return token con la información
     */
    public String generate(String username, String password, String grant_type) {

        return Jwts.builder()
                .setSubject(username)
                .claim("password", password)
                .claim("grant_type", grant_type)
                .signWith(SignatureAlgorithm.HS512, "sp900853722")
                .setExpiration(new Date(System.currentTimeMillis() + 60000))
                .compact();
    }
}
