package com.ast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Clase principal encargada de ejecutar el API
 */
@SpringBootApplication
public class App {


    /**
     * Método principal de la aplicación en el cual se inicia el API
     * @param args argumentos de la aplicación
     */
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }



}
