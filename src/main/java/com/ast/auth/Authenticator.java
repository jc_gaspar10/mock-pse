package com.ast.auth;

import com.ast.exceptions.AuthorizationException;
import com.ast.model.ResponseLoginVO;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

public class Authenticator {

    private enum User {

        CUBIDES1("Cubides1", "Manager2019#"),
        GONZALEZC("GonzalezLC", "scp@900853722");

        private final String username;
        private final String password;

        User(final String username, final String password) {
            this.username = username;
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }

        public static String getUser(final String username) {

            if (username != null) {
                for (User e : User.values()) {
                    if (username.equals(e.username)) {
                        return e.getUsername();
                    }
                }
            }
            return null;
        }

        public static boolean validUser(final String username, final String password) {

            if (username != null && password != null) {
                for (User e : User.values()) {
                    if (username.equals(e.username) && password.equals(e.password)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }


    public static ResponseLoginVO login(final String username, final String password, final String grant_type) throws AuthorizationException {

        final ResponseLoginVO responseLoginVO = new ResponseLoginVO();
        if (User.validUser(username, password)) {
            String token = Jwts.builder().setSubject(User.getUser(username))
                    .setExpiration(new Date(System.currentTimeMillis() + (60000 * 60)))
                    .signWith(SignatureAlgorithm.HS512, "sp900853722")
                    .compact();
            responseLoginVO.setAccess_token(token);
            responseLoginVO.setToken_type("bearer");
            responseLoginVO.setExpires_in(3599);
        } else {
            throw new AuthorizationException("Error");
        }
        return responseLoginVO;
    }
}
