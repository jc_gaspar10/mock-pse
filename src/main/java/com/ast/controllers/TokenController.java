package com.ast.controllers;

import com.ast.exceptions.AuthorizationException;
import com.ast.auth.Authenticator;
import com.ast.security.JwtGenerator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TokenController {

    private JwtGenerator jwtGenerator;

    public TokenController(JwtGenerator jwtGenerator) {
        this.jwtGenerator = jwtGenerator;
    }

    @RequestMapping(value = "/PSEWebAPI/login", method = RequestMethod.POST)
    public ResponseEntity login(@RequestParam final String username, @RequestParam final String password, @RequestParam final String grant_type) {

        try {

            jwtGenerator.generate(username, password, grant_type);

            return new ResponseEntity<>(Authenticator.login(username, password, grant_type), HttpStatus.OK);
        } catch (AuthorizationException e) {
            return new ResponseEntity<>("{\"error\": \"Autorization Error\", \"error_description\": \"The username or password is incorrect!\"}", HttpStatus.OK);
        }
    }
}