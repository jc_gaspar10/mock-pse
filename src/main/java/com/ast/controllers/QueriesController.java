package com.ast.controllers;

import com.ast.model.RequestQueryTransactionsVO;
import com.ast.model.ResponseQueryTransactionsVO;
import com.ast.model.TaxVO;
import com.ast.types.ReturnCode;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
public class QueriesController {

    @RequestMapping(value = "/api/queryTransactions", method = RequestMethod.POST)
    public ResponseEntity queryTransactions(@RequestBody final RequestQueryTransactionsVO request) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ResponseQueryTransactionsVO response = new ResponseQueryTransactionsVO();

        if (request.getCus() == null && request.getCusList() == null) {
            return new ResponseEntity("{\"message\": \"No se envió CUS ni una lista de CUS\"}", headers, HttpStatus.BAD_REQUEST);
        }

        if (String.valueOf(request.getCus()).endsWith("5")) {
            response.setReturnCode(ReturnCode.NOT_FOUND.getMessage());
        } else {
            if (request.getCus() != null) {
                final TaxVO tax = generateTax(request.getCus(), request.getTclass(), request.getState());
                response.getTransactions().add(tax);
                response.setReturnCode(ReturnCode.SUCCESS.getMessage());
            } else {
                for (int i = 0; i < request.getCusList().length; i++) {
                    final TaxVO tax = generateTax(request.getCusList()[i], request.getTclass(), request.getState());
                    response.getTransactions().add(tax);
                    response.setReturnCode(ReturnCode.SUCCESS.getMessage());
                }
            }
        }
        return new ResponseEntity(response, headers, HttpStatus.OK);
    }

    private TaxVO generateTax(final BigDecimal cus, final String tclass, final String state) {

        final TaxVO tax = new TaxVO();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");


        tax.setCus(cus);
        tax.setDate(sdf.format(new Date()));
        tax.setCycle(1);
        tax.setType("COMPRA");
        if (String.valueOf(cus).endsWith("4")) {
            tax.setTclass("multicredit");
        } else {
            tax.setTclass(tclass == null ? "singlecredit" : tclass);
        }
        tax.setServiceCode("1002");
        tax.setAmount(new BigDecimal((int) (Math.random() * ((50000000 - 1000000) + 1)) + 1000000));
        BigDecimal iva = tax.getAmount().multiply(new BigDecimal("0.19")).setScale(2, BigDecimal.ROUND_DOWN);
        tax.setVat(iva);
        if (String.valueOf(cus).endsWith("2")) {
            tax.setState(state == null ? generateState() : state);
        } else {
            tax.setState("approved");
        }
        tax.setSourceBankCode(String.valueOf(cus).endsWith("3") ? "1051" : "1052");
        tax.setAuthorizationId("123456");
        tax.setTargetBankCode("1021");
        tax.setCommerceIdentification("939302224");
        tax.setCommerceName("Comercio " + ((int) (Math.random() * ((10 - 1) + 1)) + 1));
        tax.setCommerceId(10001L);
        tax.setBindingType("PSE - Desarrollo Propio");
//        tax.setPuedeSolicitarReversion(((int) Math.abs(System.currentTimeMillis() % 2)) == 0 ? true : false);
        tax.setCanSolicitReversion(true);

        return tax;
    }

    private String generateState() {

        String state = "";

        switch (Math.abs((int) System.currentTimeMillis() % 2)) {

            case 0: {
                state = "rejected";
                break;
            }
            case 1: {
                state = "failed";
                break;
            }
        }

        return state;
    }

    private String generateClass() {
        String taxClass = "";

        switch (Math.abs((int) System.currentTimeMillis() % 2)) {

            case 0: {
                taxClass = "singlecredit";
                break;
            }
            case 1: {
                taxClass = "multicredit";
                break;
            }
        }

        return taxClass;
    }
}
