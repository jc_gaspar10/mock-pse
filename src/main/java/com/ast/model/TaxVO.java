package com.ast.model;

import java.math.BigDecimal;

public class TaxVO {

    private BigDecimal cus;
    private String date;
    private Integer cycle;
    private String type;
    private String tclass;
    private String serviceCode;
    private BigDecimal amount;
    private BigDecimal vat;
    private String state;
    private String sourceBankCode;
    private String authorizationId;
    private String targetBankCode;
    private String commerceIdentification;
    private String commerceName;
    private Long commerceId;
    private String bindingType;
    private boolean canSolicitReversion;

    public BigDecimal getCus() {
        return cus;
    }

    public void setCus(BigDecimal cus) {
        this.cus = cus;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getCycle() {
        return cycle;
    }

    public void setCycle(Integer cycle) {
        this.cycle = cycle;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTclass() {
        return tclass;
    }

    public void setTclass(String tclass) {
        this.tclass = tclass;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSourceBankCode() {
        return sourceBankCode;
    }

    public void setSourceBankCode(String sourceBankCode) {
        this.sourceBankCode = sourceBankCode;
    }

    public String getAuthorizationId() {
        return authorizationId;
    }

    public void setAuthorizationId(String authorizationId) {
        this.authorizationId = authorizationId;
    }

    public String getTargetBankCode() {
        return targetBankCode;
    }

    public void setTargetBankCode(String targetBankCode) {
        this.targetBankCode = targetBankCode;
    }

    public String getCommerceIdentification() {
        return commerceIdentification;
    }

    public void setCommerceIdentification(String commerceIdentification) {
        this.commerceIdentification = commerceIdentification;
    }

    public String getCommerceName() {
        return commerceName;
    }

    public void setCommerceName(String commerceName) {
        this.commerceName = commerceName;
    }

    public Long getCommerceId() {
        return commerceId;
    }

    public void setCommerceId(Long commerceId) {
        this.commerceId = commerceId;
    }

    public String getBindingType() {
        return bindingType;
    }

    public void setBindingType(String bindingType) {
        this.bindingType = bindingType;
    }

    public boolean isCanSolicitReversion() {
        return canSolicitReversion;
    }

    public void setCanSolicitReversion(boolean canSolicitReversion) {
        this.canSolicitReversion = canSolicitReversion;
    }
}
