package com.ast.model;

import java.util.ArrayList;
import java.util.Collection;

public class ResponseQueryTransactionsVO {

    private Collection<TaxVO> transactions;
    private String returnCode;

    public ResponseQueryTransactionsVO() {
        transactions = new ArrayList<TaxVO>();
    }

    public Collection<TaxVO> getTransactions() {
        return transactions;
    }

    public void setTransactions(Collection<TaxVO> transactions) {
        this.transactions = transactions;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }
}
