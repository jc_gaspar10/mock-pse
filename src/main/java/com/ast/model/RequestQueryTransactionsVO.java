package com.ast.model;

import java.math.BigDecimal;

public class RequestQueryTransactionsVO {

    private BigDecimal cus;
    private BigDecimal[] cusList;
    private String tclass;
    private String state;

    public BigDecimal getCus() {
        return cus;
    }

    public void setCus(BigDecimal cus) {
        this.cus = cus;
    }

    public BigDecimal[] getCusList() {
        return cusList;
    }

    public void setCusList(BigDecimal[] cusList) {
        this.cusList = cusList;
    }

    public String getTclass() {
        return tclass;
    }

    public void setTclass(String tclass) {
        this.tclass = tclass;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
