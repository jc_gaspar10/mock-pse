package com.ast.types;

public enum ReturnCode {

    SUCCESS("SUCCESS"),
    NOT_FOUND("NOT_FOUND"),
    INTERNAL_ERROR("INTERNAL_ERROR");

    private String message;

    ReturnCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
